package controller;

import java.util.ArrayList;

import model.data_structures.Arco;
import model.data_structures.ArregloDinamico;
import model.data_structures.Vertice;

import java.util.Iterator;
import java.util.Scanner;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.SystemMenuBar;

import model.logic.MVCModelo;
import model.logic.Maps;
import model.logic.NodoCoordenadas;
import model.logic.NodoVial;
import model.logic.TravelTime;
import model.logic.Zona;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	private final static int N=20;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
				modelo.cargarCSV();
				modelo.cargarJSONZonas();
				System.out.println("Se cargaron las zonas");
				modelo.cargarTXT();
				System.out.println("Se cargaron los vertices");
				modelo.cargarTXT2();
				System.out.println("Se ha cargado correctamente la informacion de vertices y arcos");
				System.out.println("El numero de vertices fue de: " + modelo.darVertices());
				System.out.println("El numero de arcos fue de: " + modelo.darArcos());
				ArrayList<ArrayList<Vertice>> listaCC = modelo.darGrafo().darCCPorTama�o();
				System.out.println("El numero de componentes conectadas es de: " + (listaCC.size() - 1));
				int cont = 0;
				if(listaCC.size()>6){cont = 6;}
				else{ cont = listaCC.size();}
				for(int i = 1; i<cont;i++)
				{
					if(listaCC.get(i)!=null)
					{
						ArrayList<Vertice> lista = listaCC.get(i);
						System.out.println(lista.size());
					}
				}
				break;

			case 2:
				Maps mapa = new Maps(modelo);
				System.out.println("Se cargo el mapa con la informacion de vertices y arcos");
				mapa.initFrame("Mapa Bogota");
				break;

			case 7:
				System.out.println("Ingrese la latitud inicial: ");
				Double lat1=Double.parseDouble(lector.next());
				System.out.println("Ingrese la longitud inicial: ");
				Double lon1=Double.parseDouble(lector.next());
				System.out.println("Ingrese la latitud final: ");
				Double lat2=Double.parseDouble(lector.next());
				System.out.println("Ingrese la longitud final: ");
				Double lon2=Double.parseDouble(lector.next());
				Iterator iter=modelo.caminoCostoMinimo1(lat1, lon1, lat2, lon2);
				Arco a=(Arco) iter.next();
				int totalVer=1;
				double costo=0;
				double distancia=0;
				System.out.println("ID: "+a.darinicial().darid()+ ", Long: " + a.darinicial().darInfo().darLon()+", Lat: "+a.darinicial().darInfo().darLat());
				while(iter.hasNext())
				{
					System.out.println("ID: "+a.darfinal().darid()+ ", Long: " + a.darfinal().darInfo().darLon()+", Lat: "+a.darfinal().darInfo().darLat());
					totalVer++;
					costo+=a.darCostoTiempo();
					a=(Arco) iter.next();
				}
				System.out.println("El n�mero de vertices es: "+ totalVer);
				System.out.println("El costo total es: "+ costo);
				System.out.println("La distancia es: "+ distancia);
				break;
				
			case 8:
				System.out.println("Ingrese la latitud inicial: ");
				Double latt1=Double.parseDouble(lector.next());
				System.out.println("Ingrese la longitud inicial: ");
				Double lonn1=Double.parseDouble(lector.next());
				System.out.println("Ingrese la latitud final: ");
				Double latt2=Double.parseDouble(lector.next());
				System.out.println("Ingrese la longitud final: ");
				Double lonn2=Double.parseDouble(lector.next());
				Iterator iterr=modelo.caminoCostoMinimo2(latt1, lonn1, latt2, lonn2);
				Arco aa=(Arco) iterr.next();
				int totalVerr=1;
				double costoo=0;
				double distanciaa=0;
				System.out.println("ID: "+aa.darinicial().darid()+ ", Long: " + aa.darinicial().darInfo().darLon()+", Lat: "+aa.darinicial().darInfo().darLat());
				while(iterr.hasNext())
				{
					System.out.println("ID: "+aa.darfinal().darid()+ ", Long: " + aa.darfinal().darInfo().darLon()+", Lat: "+aa.darfinal().darInfo().darLat());
					totalVerr++;
					costoo+=aa.darCostoHaver();
					aa=(Arco) iterr.next();
				}
				System.out.println("El n�mero de vertices es: "+ totalVerr);
				System.out.println("El costo total es: "+ costoo);
				System.out.println("La distancia es: "+ distanciaa);
				break;
				
			default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
