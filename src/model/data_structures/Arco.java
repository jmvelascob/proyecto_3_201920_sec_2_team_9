package model.data_structures;

public class Arco implements Comparable<Arco> {
	
		private double costoHaver;
		private double costoTiempo;
		private double costoVelocidad;
		private transient Vertice inicial;
		private transient Vertice finall;
		
		public Arco(double pCostoHaver, Vertice pInicial, Vertice pFinal)
		{
			costoHaver=pCostoHaver;
			inicial=pInicial;
			finall=pFinal;
		}
		
		public Vertice darinicial()
		{
			return inicial;
		}
		
		public Vertice darfinal()
		{
			return finall;
		}
		
		public double darCostoHaver()
		{
			return costoHaver;
		}
		
		public double darCostoTiempo()
		{
			return costoTiempo;
		}
		
		public double darCostoVel()
		{
			return costoVelocidad;
		}
		public void cambiarCostoHaver(double cost)
		{
			costoHaver=cost;
		}
		public void cambiarCostoTiempo(double cost)
		{
			costoTiempo=cost;
			costoVelocidad = costoHaver / costoTiempo;
		}

		@Override
		public int compareTo(Arco o) {
			// TODO Auto-generated method stub
			return 0;
		}
}