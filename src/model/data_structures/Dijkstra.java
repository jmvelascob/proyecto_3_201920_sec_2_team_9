package model.data_structures;

import java.util.Iterator;
import java.util.Stack;

public class Dijkstra {
	

    private double[] distTo;          // distTo[v] = distance  of shortest s->v path
    private Arco[] edgeTo;    // edgeTo[v] = last edge on shortest s->v path
    private IndexMinPQ<Double> pq;    // priority queue of vertices

    public Dijkstra(Graph G,Vertice s) {
    	
    	 distTo = new double[G.darV()];
         edgeTo = new Arco[G.darV()];
         validateVertex(s);
         for (int v = 0; v < G.darV(); v++)
            distTo[v] = Double.POSITIVE_INFINITY;
         	distTo[s.drmas()] = 0.0;
         	
            // relax vertices in order of distance from s
            pq = new IndexMinPQ<Double>(G.darV());
            pq.insert(s.drmas(), distTo[s.drmas()]);
            while (!pq.isEmpty()) {
                int v = pq.delMin();
                //ENCONTRAR VERTICE
                Iterator iter =G.darArcos().iterator();
                Arco ar=(Arco)iter.next();
                Vertice ver=null;
               while(iter.hasNext())
               {
            	   if(ar.darinicial().drmas()==v)
            	   {
            		   ver=ar.darinicial();
            	   }
            	   if(ar.darfinal().drmas()==v)
            	   {
            		   ver=ar.darfinal();
            	   }
            	   ar=(Arco)iter.next();
               }
               
               Iterator iterr=ver.iterator();
               Arco e=(Arco)iterr.next();
               while(iter.hasNext())
               {
            	   relax(e);
            	   e=(Arco)iterr.next();
               }
            }
            
    }
    private void relax(Arco e) {
		// TODO Auto-generated method stub
    	
        int v = e.darinicial().drmas(), w = e.darfinal().drmas();
        if (distTo[w] > distTo[v] + e.darCostoTiempo()) {
            distTo[w] = distTo[v] + e.darCostoTiempo();
            edgeTo[w] = e;
            if (pq.contains(w)) pq.decreaseKey(w, distTo[w]);
            else                pq.insert(w, distTo[w]);
        }
	}
    
    public double distTo(Vertice vw) {
    	 int v=vw.drmas();
        validateVertex(vw);
        return distTo[v];
    }
    
    public boolean hasPathTo(Vertice vw) {
        validateVertex(vw);
        int v=vw.drmas();
        return distTo[v] < Double.POSITIVE_INFINITY;
    }
    
    public Iterable<Arco> pathTo(Vertice vw) {
        validateVertex(vw);
        int v=vw.drmas();
        if (!hasPathTo(vw)) return null;
        Stack<Arco> path = new Stack<Arco>();
        for (Arco e = edgeTo[v]; e != null; e = edgeTo[e.darinicial().drmas()]) {
            path.push(e);
        }
        return path;
    }

	// throw an IllegalArgumentException unless {@code 0 <= v < V}
    private void validateVertex(Vertice vw) {
        int V = distTo.length;
        int v=vw.drmas();
        if (v < 0 || v >= V)
            throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
    }
}
