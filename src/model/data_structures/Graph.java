package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

public class Graph <K extends Comparable<K>,V extends Comparable<V>>{

	
	public int V;
	public int E;
	private int CC;
	public HashLinearProbing<K,V> hash;
	public Cola<Arco> arcos;
	
	public Graph(int v)
	{
		hash=new HashLinearProbing<K,V>(v);
		CC = 0;
	}
	
	public boolean tieneV(K k)
	{
		return hash.contains(k);
	}
	
	public int darV()
	{
		return V;
	}
	
	public int darE()
	{
		return E;
	}
	
	public Cola<Arco> darArcos()
	{
		return arcos;
	}
	public HashLinearProbing<K,V> darHash()
	{
		return hash;
	}
	
	public void addEdge(K idVertexIni, K idVertexFin, double cost, double costTiempo)
	{
		if(!tieneV(idVertexIni)|| !tieneV(idVertexFin))
		{
			System.out.println("No hay vertice");
		}
		else{
			Vertice ini = (Vertice)hash.get(idVertexIni);
			Vertice fin = (Vertice)hash.get(idVertexFin);
			Arco i = new Arco(cost, ini, fin);
			i.cambiarCostoTiempo(costTiempo);
			Arco f = new Arco(cost, fin, ini);
			f.cambiarCostoTiempo(costTiempo);
			ini.agregarArco(i);
			fin.agregarArco(f);
			E++;
		}
		
	}
	
	public V getInfoVertex(K idVertex)
	{
		if(!hash.contains((idVertex)))
			return null;
		else
		{
			Vertice v=(Vertice) hash.get(idVertex);
			V p=(V) v.darInfo();
			return p;
		}
		
}
	
	public void setInfoVertex(K idVertex, V infoVertex)
	{
		if(!hash.contains(idVertex))
			System.out.println("No existe");
		else
		{
			Vertice v=(Vertice) hash.get(idVertex);
			v.cambiarInfo((InfoVer)infoVertex);
		}
	}
	
	public double getCostArc(K idVertexIni, K idVertexFin)
	{
		int cont=0;
		Node<Arco> a=arcos.darPrimer();
		double cc=-1;
		while(cont<arcos.darTama�o())
		{
			if((int)a.darData().darinicial().darid()==((int)idVertexIni)&&(int)a.darData().darfinal().darid()==((int)idVertexFin))
			{
				cc= a.darData().darCostoHaver();
			}
				a=a.darSiguiente();
			cont++;
		}
		return cc;
	}
	
	public void setCostArc(K idVertexIni, K idVertexFin, double cost)
	{
		int cont=0;
		Node<Arco> a=arcos.darPrimer();
		while(cont<arcos.darTama�o())
		{
			if((int)a.darData().darinicial().darid()==((int)idVertexIni)&&(int)a.darData().darfinal().darid()==((int)idVertexFin))
			{
				a.darData().cambiarCostoHaver(cost);
			}
				a=a.darSiguiente();
				cont++;
		}
	}
	
	public void addVertex(K idVertex, V infoVertex)
	{
		Vertice ver = new Vertice(idVertex, (InfoVer)infoVertex);
		V x = (V) ver;
		hash.putInSet(idVertex, x);
		V++;
		ver.mass(V);
	}
	
	public Iterable <K> adj (K idVertex)
	{
		Vertice ver= (Vertice)hash.get( idVertex);
		return (Iterable<K>) ver.iterator();
	}
	
	public void uncheck()
	{
		Iterator<K> llaves=(Iterator<K>) hash.keys();
		while(llaves.hasNext())
		{
			K llave=llaves.next();
			Vertice v=(Vertice)hash.get(llave);
			v.unsetVisitado();
		}
	}
	 
	public void dfs(K s)
	{
		Vertice vertice = (Vertice)hash.get(s);
		Iterator<Arco> iter = vertice.iterator();
		if(vertice.esVisitado()==true)
		{
			return;
		}
		else
		{
			vertice.setVisitado(CC);
			while(iter.hasNext())
			{
				Arco arc = iter.next();
				K id = (K)arc.darfinal().darid();
				dfs(id);
			}
		}
	}
	
	public int cc()
	{
		CC = 0;
		uncheck();
		Iterator keys = hash.keys();
		while(keys.hasNext())
		{
			K key = (K) keys.next();
			Vertice vert = (Vertice)hash.get(key);
			if(vert.esVisitado()==false)
			{
				CC = CC + 1;
			}
			dfs(key);
		}
		return CC;
	}
	
	public Iterator<K> getCC(K idVertex)
	{
		ArrayList<Vertice> arr = new ArrayList<Vertice>();
		cc();
		Vertice ini = (Vertice)hash.get(idVertex);
		int arr2 = ini.darCC();
		if(arr2==0)
		{
			return (Iterator<K>) arr.iterator();
		}
		else
		{
			int ccIni = arr2;
			Iterator keys = hash.keys();
			while(keys.hasNext())
			{
				K key = (K) keys.next();
				Vertice vert = (Vertice)hash.get(key);
				int ccVer = vert.darCC();
				if(ccVer!=0 && ccVer == ccIni)
				{
					arr.add(vert);
				}
			}
			return (Iterator<K>) arr.iterator();
		}
	}
	
	private static double distancia(double startLat, double startLong, double endLat, double endLong) 
	{

		double dLat  = Math.toRadians((endLat - startLat));
		double dLong = Math.toRadians((endLong - startLong));

		startLat = Math.toRadians(startLat);
		endLat   = Math.toRadians(endLat);

		double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return 6371 * c;
	}

	private static double haversin(double val) 
	{
		return Math.pow(Math.sin(val / 2), 2);
	}
	
	public Comparable darVerticeMasCercano(double lon, double lat)
	{
		Iterator keys = hash.keys();
		double menor = 9999999;
		Vertice resp = null;
		while(keys.hasNext())
		{
			K key = (K) keys.next();
			Vertice vert = (Vertice) hash.get(key);
			double dist = distancia(vert.darInfo().darLat(), vert.darInfo().darLon(), lat, lon);
			if(dist<menor)
			{
				menor = dist;
				resp = vert;
			}
		}
		return resp.darid();
	}
	public Vertice darVerticeMasCercanoo(double lon, double lat)
	{
		Iterator keys = hash.keys();
		double menor = 9999999;
		Vertice resp = null;
		while(keys.hasNext())
		{
			K key = (K) keys.next();
			Vertice vert = (Vertice) hash.get(key);
			double dist = distancia(vert.darInfo().darLat(), vert.darInfo().darLon(), lat, lon);
			if(dist<menor)
			{
				menor = dist;
				resp = vert;
			}
		}
		return resp;
	}
	
	public ArrayList<ArrayList<Vertice>> darCCPorTama�o()
	{
		ArrayList<ArrayList<Vertice>> listaCC = new ArrayList<ArrayList<Vertice>>();
		cc();
		for(int j=0;j<=CC;j++)
		{
			ArrayList<Vertice> listaVertices = new ArrayList<Vertice>();
			listaCC.add(listaVertices);
		}
		Iterator keys = hash.keys();
		int cont = 0;
		while(keys.hasNext())
		{
			K key = (K) keys.next();
			Vertice vert = (Vertice) hash.get(key);
			for(int i=1; i<=CC;i++)
			{
				if(vert.darCC() == i)
				{
					ArrayList<Vertice> lista = listaCC.get(i);
					lista.add(vert);
					listaCC.set(i,lista);
				}
			}
		}
		ArrayList<ArrayList<Vertice>> nuevaOrdenada = new ArrayList<ArrayList<Vertice>>();
		ArrayList<Vertice> nula = new ArrayList<Vertice>();
		nuevaOrdenada.add(nula);
		for(int i=1;i<listaCC.size();i++)
		{
			ArrayList<Vertice> mayor = new ArrayList<Vertice>();
			int indice = 0;
			for(int j=1;j<listaCC.size();j++)
			{
				ArrayList<Vertice> list = listaCC.get(j);
				if(list.size() > mayor.size())
				{
					mayor = list;
					indice = j;
				}
			}
			ArrayList<Vertice> anadir = listaCC.get(indice);
			nuevaOrdenada.add(anadir);
			ArrayList<Vertice> vacia = new ArrayList<Vertice>();
			listaCC.set(indice, vacia);
		}
		return nuevaOrdenada;
	}
}
	