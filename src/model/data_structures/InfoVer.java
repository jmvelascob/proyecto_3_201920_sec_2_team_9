package model.data_structures;

public class InfoVer implements Comparable<InfoVer> {

	private int id;
	private double longitud;
	private double latitud;
	private int movementID;
	
	public InfoVer(int idd, double lon, double lat, int mov)
	{
		id = idd;
		longitud = lon;
		latitud = lat;
		movementID = mov;
	}
	
	public int darId()
	{
		return id;
	}
	
	public double darLon()
	{
		return longitud;
	}

	public double darLat()
	{
		return latitud;
	}
	
	public int darMov()
	{
		return movementID;
	}
	@Override
	public int compareTo(InfoVer arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
}
