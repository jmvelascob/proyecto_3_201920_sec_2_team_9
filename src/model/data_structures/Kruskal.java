package model.data_structures;

import java.util.Iterator;

public class Kruskal {
	
    private static final double FLOATING_POINT_EPSILON = 1E-12;

    private double weight;                        // weight of MST
    private Cola<Arco> mst = new Cola<Arco>();  // edges in MST
    /**
     * Compute a minimum spanning tree (or forest) of an edge-weighted graph.
     * @param G the edge-weighted graph
     */
    public Kruskal(Graph G) {
        // more efficient to build heap by passing array of edges
        MinPQ<Arco> pq = new MinPQ<Arco>();
        Cola<Arco> todosArcos=G.darArcos();
        Iterator iter= todosArcos.iterator();
        Arco e=(Arco) iter.next();
        while(iter.hasNext())
        {
            pq.insert(e);
        }
        // run greedy algorithm
        uf uf = new uf(G.darV());
        while (!pq.isEmpty() && mst.darTama�o() < G.darV() - 1) {
            Arco d = pq.delMin();
            Vertice v = d.darinicial();
            Vertice w = d.darfinal();
            if (uf.find(v.drmas()) != uf.find(w.drmas())) { // v-w does not create a cycle
                uf.union(v.drmas(), w.drmas());  // merge v and w components
                mst.enqueue(d);  // add edge e to mst
                weight += d.darCostoHaver();
            }
        }
    }
    
    /**
     * Returns the edges in a minimum spanning tree (or forest).
     * @return the edges in a minimum spanning tree (or forest) as
     *    an iterable of edges
     */
    public Cola<Arco> edges() {
        return mst;
    }

    /**
     * Returns the sum of the edge weights in a minimum spanning tree (or forest).
     * @return the sum of the edge weights in a minimum spanning tree (or forest)
     */
    public double weight() {
        return weight;
    }
    }
