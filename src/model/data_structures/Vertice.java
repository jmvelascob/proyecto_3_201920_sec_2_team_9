package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

public class Vertice <K extends Comparable<K>> implements Comparable
{

	private int visitado;
	private ArrayList<Arco> vecinos;
	private InfoVer info;
	private K id;
	private Vertice origen;
	private int mas;
	
	public Vertice(K idd, InfoVer i)
	{
		visitado=0;
		vecinos=new ArrayList<Arco>();
		id=idd;
		info=i;
	}
	
	public K darid()
	{
	return id;
	}
	public InfoVer darInfo()
	{
		return info;
	}
	
	public void cambiarInfo(InfoVer lo)
	{
		info=lo;
	}
	public boolean esVisitado()
	{
		if(visitado==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public void agregarArco(Arco v)
	{
		vecinos.add(v);
	}
	
	public boolean esVecino(Arco v)
	{
		return vecinos.contains(v);
	}
	
	public int numVEC()
	{
		return vecinos.size();
	}
	
	public void eliminarVecino(Arco v)
	{
		vecinos.remove(v);
	}
	
	public int darCC()
	{
		return visitado;
	}
	
	public void setVisitado(int x)
	{
		visitado = x;
	}
	public void unsetVisitado()
	{
		visitado = 0;
	}
	
	public Iterator<Arco> iterator() 
	{
		return vecinos.iterator();
	}
	
	public void cambiarOrigen(Vertice pOrigen)
	{
		origen = pOrigen;
	}
	
	public void reiniciarOrigen()
	{
		origen = null;
	}
	
	public Vertice darOrigen()
	{
		return origen;
	}

	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void mass(int v) {
		// TODO Auto-generated method stub
		mas=v;
	}
	
	public int drmas()
	{
		return mas;
	}


}