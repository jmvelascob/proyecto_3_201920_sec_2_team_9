package model.logic;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.opencsv.CSVReader;

import model.data_structures.Arco;
import model.data_structures.ArregloDinamico;
import model.data_structures.Cola;
import model.data_structures.Dijkstra;
import model.data_structures.Dijkstra2;
import model.data_structures.Graph;
import model.data_structures.HashLinearProbing;
import model.data_structures.InfoVer;
import model.data_structures.Vertice;

/**
 * Definicion del modelo del mundo
 * @param <K>
 * @param <K>
 * @param <K>
 *
 */
public class MVCModelo<K extends Comparable<K>,V extends Comparable<V>> {
	
	private Zona zona;
	private int contadorZonas;
	private String instruccion;
	private ArregloDinamico<Zona> arr1;
	private ArregloDinamico<NodoVial> txt;
	private HashLinearProbing<K,V> viajes;
	private Graph grafo;
	private String path;
	int mayorSource;
	private ArrayList<Integer> arr;
	private ArrayList<Double> a;

	
	public MVCModelo()
	{
		txt=new ArregloDinamico<NodoVial>(10000);
		arr1= new  ArregloDinamico<Zona>(10000);
		grafo = new Graph(200);
		arr = new ArrayList<Integer>();
		a = new ArrayList<Double>();
		viajes = new HashLinearProbing<K,V>(10000);
	}
	
	public void cargarCSV()
	{
		CSVReader reader = null;
		try {
			
		
			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-WeeklyAggregate.csv"));

			try{
				String[] x =reader.readNext();

			}
			catch(Exception e)
			{
				System.out.println("Problemas leyendo la primera linea");
			}
			
			for(String[] nextLine : reader) 
			{
				TravelTime dato = new TravelTime( 1 ,Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
				K llave = (K) (dato.darSourceid() + "," + dato.darDtid());
				V dat = (V)dato;
				viajes.putInSet(llave, dat);

				if(dato.darSourceid()>mayorSource)
				{
					mayorSource = dato.darSourceid();
				}
			}
			System.out.println("N�mero de viajes archivo horas trimestre 1: "+ viajes.darDatos());
		}
		
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		finally
		{
			if (reader != null) 
			{
				try 
				{
					reader.close();

				} catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	// R5
	public Iterator caminoCostoMinimo1(double lat1, double lon1, double lat2, double lon2 )
	{
		Vertice verticeI= (Vertice) grafo.darVerticeMasCercano(lat1, lon1);
		Vertice verticeF = (Vertice) grafo.darVerticeMasCercano(lat2, lon2);
		Dijkstra dij= new Dijkstra(grafo, verticeI);
		return (Iterator)dij.pathTo(verticeF);
		
	}
	public Iterator caminoCostoMinimo2(double lat1, double lon1, double lat2, double lon2 )
	{
		Vertice verticeI= (Vertice) grafo.darVerticeMasCercanoo(lat1, lon1);
		Vertice verticeF = (Vertice) grafo.darVerticeMasCercanoo(lat2, lon2);
		Dijkstra2 dij= new Dijkstra2(grafo, verticeI);
		return (Iterator)dij.pathTo(verticeF);
		
	}
	
	
//	public NodoVial aproximacion(double lat, double longg)
//	{
//		int cont=0;
//		double menordis=10000000;
//		NodoVial nodo=null;
//		while (cont<txt.darTamano())
//		{
//			NodoVial vertice=txt.darElemento(cont);
//			double reslat= vertice.darlatitud()-lat;
//			double reslong= vertice.darlongitud()-longg;
//			double dis= Math.abs(reslat)+Math.abs(reslong);
//			if (lat==vertice.darlatitud() && longg==vertice.darlongitud())
//			{
//				nodo=vertice;
//				break;
//			}
//			else if(dis<menordis)
//			{
//				menordis=dis;
//				nodo=vertice;
//			}
//
//		}
//		return nodo;
//	}

	
	



	public void cargarTXT()
	{
		int numt=0;
		int cont=0;
		CSVReader reader = null;
		try
		{
			reader = new CSVReader(new FileReader("./data/bogota_vertices.txt"));
			try
			{
				String[] x =reader.readNext();
			}
			catch(Exception e)
			{
				System.out.println("Problemas leyendo la primera linea");
			}
			for(String[] nextLine : reader) 
			{
				System.out.println("DFGJNAGEMT");
				String[] ar = nextLine[0].split(";");
				InfoVer info = new InfoVer(Integer.parseInt(ar[0]), Double.parseDouble(ar[1]), Double.parseDouble(ar[2]), Integer.parseInt(ar[3]));
				Vertice ver = new Vertice(Integer.parseInt(ar[0]), info);
				grafo.addVertex(Integer.parseInt(ar[0]), info);
				numt++;
				System.out.println(numt);
				if(numt==3000)
				{
					System.out.println("s");
					break;
				}
			}
		}
		
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		finally
		{
			if (reader != null  ) 
			{
				try 
				{
					reader.close();


				} catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}

	public void cargarTXT2()
	{
		int numt=0;
		CSVReader reader = null;
		try
		{
			reader = new CSVReader(new FileReader("./data/bogota_arcos.txt"));
			for(String[] nextLine : reader) 
			{
				String[] ar = nextLine[0].split(" ");
				for(int i=1;i<ar.length;i++)
				{
					int ini = Integer.parseInt(ar[0]);
					int fin = Integer.parseInt(ar[i]);
					if(grafo.tieneV(ini) && grafo.tieneV(fin))
					{
						InfoVer infoIni = (InfoVer) grafo.getInfoVertex(ini);
						InfoVer infoFin = (InfoVer) grafo.getInfoVertex(fin);
						double dist = 1000 * distancia(infoIni.darLat(), infoIni.darLon(), infoFin.darLat(), infoFin.darLon());
						Vertice vert1 = (Vertice)grafo.darHash().get(ini);
						Vertice vert2 = (Vertice)grafo.darHash().get(fin);
						int mov1 = vert1.darInfo().darMov();
						int mov2 = vert2.darInfo().darMov();
						double tiempo = 0;
						if(mov1==mov2)
						{
							K key = (K) (mov1 + "," + mov2);
							K key2 = (K) (mov2 + "," + mov1);
							double total  = 0;
							int cantidadViajes=0;
							if(viajes.contains(key))
							{
								Iterator iter1 = viajes.getSet(key);
								while(iter1.hasNext())
								{
									TravelTime viaje = (TravelTime) iter1.next();
									total = total + viaje.darMean_travel_time();
									cantidadViajes++;
								}
							}
							if(viajes.contains(key2))
							{
								Iterator iter1 = viajes.getSet(key2);
								while(iter1.hasNext())
								{
									TravelTime viaje = (TravelTime) iter1.next();
									total = total + viaje.darMean_travel_time();
									cantidadViajes++;
								}
							}
							if(cantidadViajes!=0)
							{
								tiempo = total / cantidadViajes;
							}
							if(tiempo==0)
							{
								tiempo = 10;
							}
						}
						else
						{
							K key = (K) (mov1 + "," + mov2);
							K key2 = (K) (mov2 + "," + mov1);
							double total  = 0;
							int cantidadViajes=0;
							if(viajes.contains(key))
							{
								Iterator iter1 = viajes.getSet(key);
								while(iter1.hasNext())
								{
									TravelTime viaje = (TravelTime) iter1.next();
									total = total + viaje.darMean_travel_time();
									cantidadViajes++;
								}
							}
							if(viajes.contains(key2))
							{
								Iterator iter1 = viajes.getSet(key2);
								while(iter1.hasNext())
								{
									TravelTime viaje = (TravelTime) iter1.next();
									total = total + viaje.darMean_travel_time();
									cantidadViajes++;
								}
							}
							if(cantidadViajes!=0)
							{
								tiempo = total / cantidadViajes;
							}
							if(tiempo==0)
							{
								tiempo = 100;
							}
						}
						grafo.addEdge(ini, fin, dist, tiempo);
					}
					
				}
				numt++;
			}
		}
		
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		finally
		{
			if (reader != null  ) 
			{
				try 
				{
					reader.close();


				} catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	public static double distancia(double startLat, double startLong, double endLat, double endLong) 
	{

		double dLat  = Math.toRadians((endLat - startLat));
		double dLong = Math.toRadians((endLong - startLong));

		startLat = Math.toRadians(startLat);
		endLat   = Math.toRadians(endLat);

		double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return 6371 * c;
	}

	public static double haversin(double val) 
	{
		return Math.pow(Math.sin(val / 2), 2);
	}
	
	public void crearJSON()
	{
		Gson gson = new Gson();
		try
		{
			FileWriter fileW = new FileWriter("./data/grafoJSON.json");
			BufferedWriter bufferedW = new BufferedWriter(fileW);
			bufferedW.write(gson.toJson(grafo));
			bufferedW.close();
		}
		catch(Exception e)
		{
			System.out.println("Error al crear el archivo JSON- " + e.getMessage());
		}
	}
	
	public void cargarJSON()
	{
		try
		{
			JsonParser parser = new JsonParser();
			String path = "./data/grafoJSON.json";
	        FileReader fr = new FileReader(path);
	        JsonElement datos = parser.parse(fr);
	        String x = datos.toString();
	        Gson gson = new Gson();
	        grafo = gson.fromJson(x, Graph.class);
		}
		catch(Exception e)
		{
			System.out.println("No se pudo leer el json " + e.getMessage());
		} 
	}
	
	public Graph darGrafo()
	{
		return grafo;
	}

	public int darVertices()
	{
		return grafo.darV();
	}
	
	public int darArcos()
	{
		return grafo.darE();
	}
	
	
	
	public void cargarJSONZonas() 
	{
		try
		{
			JsonParser parser = new JsonParser();
			String path = "./data/bogota_cadastral.json";
	        FileReader fr = new FileReader(path);
	        JsonElement datos = parser.parse(fr);
	        json(datos);
		}
		catch(Exception e)
		{
			System.out.println("No se pudo leer el json");
		}
	}
	public void json(JsonElement elemento) {
	    if (elemento.isJsonObject()) {
	       // System.out.println("Es objeto");
	        JsonObject obj = elemento.getAsJsonObject();
	        java.util.Set<java.util.Map.Entry<String,JsonElement>> entradas = obj.entrySet();
	        java.util.Iterator<java.util.Map.Entry<String,JsonElement>> iter = entradas.iterator();
	        while (iter.hasNext()) 
	        {
	            java.util.Map.Entry<String,JsonElement> entrada = iter.next();
	           // System.out.println("Clave: " + entrada.getKey());
	            if(entrada.getKey().equals("coordinates"))
	            {
	            	instruccion = "coordinates";
	            	zona = new Zona();
	            	contadorZonas++;
	            }
	            else if(entrada.getKey().equals("type"))
	            {
	            	instruccion = "type";
	            }
	            else if(entrada.getKey().equals("geometry"))
	            {
	            	instruccion = "geometry";
	            }
	            else if(entrada.getKey().equals("cartodb_id"))
	            {
	            	instruccion = "cartodb_id";
	            }
	            else if(entrada.getKey().equals("scacodigo"))
	            {
	            	instruccion = "scacodigo";
	            }
	            else if(entrada.getKey().equals("scatipo"))
	            {
	            	instruccion = "scatipo";
	            }
	            else if(entrada.getKey().equals("scanombre"))
	            {
	            	instruccion = "scanombre";
	            }
	            else if(entrada.getKey().equals("shape_leng"))
	            {
	            	instruccion = "shape_leng";
	            }
	            else if(entrada.getKey().equals("shape_area"))
	            {
	            	instruccion = "shape_area";
	            }
	            else if(entrada.getKey().equals("MOVEMENT_ID"))
	            {
	            	instruccion = "MOVEMENT_ID";
	            }
	            else if(entrada.getKey().equals("DISPLAY_NAME"))
	            {
	            	instruccion = "DISPLAY_NAME";
	            }
	            else if(entrada.getKey().equals("properties"))
	            {
	            	instruccion="properties";
	            }
	            //System.out.println("Valor:");
	            json(entrada.getValue());
	        }
	 
	    } else if (elemento.isJsonArray()) {
	        JsonArray array = elemento.getAsJsonArray();
	        //System.out.println("Es array. Numero de elementos: " + array.size());
	        java.util.Iterator<JsonElement> iter = array.iterator();
	        while (iter.hasNext()) {
	            JsonElement entrada = iter.next();
	            json(entrada);
	        }
	    } else if (elemento.isJsonPrimitive()) {
	       // System.out.println("Es primitiva");
	        JsonPrimitive valor = elemento.getAsJsonPrimitive();
	        if (valor.isBoolean())
	        {
	           // System.out.println("Es booleano: " + valor.getAsBoolean());
	        } 
	        else if (valor.isNumber()) 
	        {	
	        	if(instruccion.equals("coordinates"))
	        	{
	        		agregarCoordenadas(valor.getAsDouble());
	        	}
	        	else if(instruccion.equals("MOVEMENT_ID"))
	        	{
	        		agregarMovementID(valor.getAsInt());
	        	}
	        	else if(instruccion.equals("shape_leng"))
	            {
	            	agregarShapeLen(valor.getAsDouble());
	            }
	            else if(instruccion.equals("shape_area"))
	            {
	            	agregarShapeArea(valor.getAsDouble());
	            }
	            //System.out.println("Es numero: " + valor.getAsNumber());
	            
	        } else if (valor.isString()) 
	        {
	        	if(instruccion.equals("scanombre"))
	        	{
	        		agregarSCANombre(valor.getAsString());
	        	}
	        	else if (instruccion.equals("MOVEMENT_ID"))
	        	{
	        		agregarMovementID(valor.getAsInt());
	        	}
	            //System.out.println("Es texto: " + valor.getAsString());
	        }
	    } else if (elemento.isJsonNull()) {
	        System.out.println("Es NULL");
	    } else {
	        System.out.println("Es otra cosa");
	    }
	}
	
	public void agregarCoordenadas(double valor)
	{
		NodoCoordenadas primer = zona.darPrimerasCoordenadas();
		while(primer.darSiguiente()!=null)
		{
			primer = primer.darSiguiente();
		}
		if(primer.darLongitud()==0)
		{
			primer.cambiarLongitud(valor);
		}
		else if(primer.darLatitud()==0)
		{
			primer.cambiarLatitud(valor);
			NodoCoordenadas nuevo = new NodoCoordenadas();
			primer.cambiarSiguiente(nuevo);
		}
	}
	private int numjson=0;
	public void agregarMovementID(int valor)
	{
		zona.cambiarMovementID(valor);
		arr1.agregar(zona);
		zona.ponerleNombreNodos();
		numjson++;
	}
	
	public void agregarSCANombre(String valor)
	{
		zona.cambiarSCANombre(valor);
	}
	
	public void agregarShapeLen(double valor)
	{
		zona.cambiarShapeLen(valor);
	}
	
	public void agregarShapeArea(double valor)
	{
		zona.cambiarShape_Area(valor);
	}
	
}

