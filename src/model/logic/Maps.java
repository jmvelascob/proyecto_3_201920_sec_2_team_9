package model.logic;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JFrame;

import com.teamdev.jxmaps.Circle;
import com.teamdev.jxmaps.CircleOptions;
import com.teamdev.jxmaps.ControlPosition;
import com.teamdev.jxmaps.InfoWindow;
import com.teamdev.jxmaps.InfoWindowOptions;
import com.teamdev.jxmaps.LatLng;
import com.teamdev.jxmaps.Map;
import com.teamdev.jxmaps.MapOptions;
import com.teamdev.jxmaps.MapReadyHandler;
import com.teamdev.jxmaps.MapStatus;
import com.teamdev.jxmaps.MapTypeControlOptions;
import com.teamdev.jxmaps.Marker;
import com.teamdev.jxmaps.Polyline;
import com.teamdev.jxmaps.PolylineOptions;
import com.teamdev.jxmaps.swing.MapView;

import model.data_structures.Arco;
import model.data_structures.Graph;
import model.data_structures.HashLinearProbing;
import model.data_structures.Vertice;

public class Maps<K extends Comparable<K>,V extends Comparable<V>> extends MapView {

	// Objeto Google Maps
	private Map map;
	private MVCModelo modelo;
	
	//Coordenadas del camino a mostrar (secuencia de localizaciones (Lat, Long))
	private LatLng[] locations = {new LatLng(4.6285797,-74.0649341), new LatLng(4.608550, -74.076443), new LatLng(4.601363, -74.0661), new LatLng(4.5954979,-74.068708) }; //Coordenadas de los vertices inicio, intermedio y fin.		

	public Maps(MVCModelo modelo)
	{	
		Graph grafo = modelo.darGrafo();
		HashLinearProbing<K,V> hash = grafo.darHash();
		setOnMapReadyHandler( new MapReadyHandler() {
				@Override
				public void onMapReady(MapStatus status)
				{
			         if ( status == MapStatus.MAP_STATUS_OK )
			         {
			        	 map = getMap();

			        	 // Configuracion de localizaciones intermedias del path (circulos)
			        	 CircleOptions middleLocOpt= new CircleOptions(); 
			        	 middleLocOpt.setFillColor("#00FF00");  // color de relleno
			        	 middleLocOpt.setFillOpacity(0.5);
			        	 middleLocOpt.setStrokeWeight(1.0);
			        	 
			        	//Configuracion de la linea del camino
			        	 PolylineOptions pathOpt = new PolylineOptions();
			        	 pathOpt.setStrokeColor("#0000FF");	  // color de linea	
			        	 pathOpt.setStrokeOpacity(1.75);
			        	 pathOpt.setStrokeWeight(1.5);
			        	 pathOpt.setGeodesic(false);

			        	 // Localizacion intermedia 1
			        	
			        	 Iterator keys = hash.keys();
			        	 int cont = 0;
			        	 while(keys.hasNext() && cont<500)
			        	 {
			        		 K key = (K) keys.next();
			        		 Vertice vert = (Vertice) hash.get(key);
			        		 double lon = vert.darInfo().darLon();
			        		 double lat = vert.darInfo().darLat();
			        		 if(lon>=-74.094723 && lon<=-74.062707 && lat>=4.597714 && lat<=4.621360)
			        		 {
			        			 LatLng coor = new LatLng(vert.darInfo().darLat(), vert.darInfo().darLon());
				        		 Circle middleLoc1 = new Circle(map);
					        	 middleLoc1.setOptions(middleLocOpt);
					        	 middleLoc1.setCenter(coor); 
					        	 middleLoc1.setRadius(20);
					        	 Iterator ady = vert.iterator();
					        	 ArrayList<LatLng> ar = new ArrayList<LatLng>();
					        	 ar.add(coor);
					        	 while(ady.hasNext())
					        	 {
					        		 Arco adycc = (Arco) ady.next();
					        		 Vertice adyc = adycc.darfinal();
					        		 lon = adyc.darInfo().darLon();
					        		 lat = adyc.darInfo().darLat();
					        		 if(lon>=-74.094723 && lon<=-74.062707 && lat>=4.597714 && lat<=4.621360)
					        		 {
					        			 coor = new LatLng(lat, lon);
					        			 ar.add(coor);
					        		 }
					        	 }
					        	 LatLng[] locat = new LatLng[ar.size()];
					        	 for(int i=0;i<ar.size();i++)
					        	 {
					        		 locat[i] = ar.get(i);
					        	 }
					        	 
					        	// Linea del camino
					        	 Polyline path = new Polyline(map); 														
					        	 path.setOptions(pathOpt); 
					        	 path.setPath(locat);
					        	 cont++;
			        		 }
			        	 }
			        	 initMap( map );
			         }
				}

		} );
		
				
	}
	
	public void initMap(Map map)
	{
		MapOptions mapOptions = new MapOptions();
		MapTypeControlOptions controlOptions = new MapTypeControlOptions();
		controlOptions.setPosition(ControlPosition.BOTTOM_LEFT);
		mapOptions.setMapTypeControlOptions(controlOptions);

		map.setOptions(mapOptions);
        map.setCenter(locations[2]);
		map.setZoom(14.0);
		
	}
	
	public void initFrame(String titulo)
	{
		JFrame frame = new JFrame(titulo);
		frame.setSize(800, 800);
		frame.add(this, BorderLayout.CENTER);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}
