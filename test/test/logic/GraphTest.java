package test.logic;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Graph;
import model.data_structures.InfoVer;
import model.data_structures.Vertice;

public class GraphTest<K extends Comparable <K>,V extends Comparable <V>> {

	private Graph<Integer, V> graf;
	InfoVer a = new InfoVer(1, 1, 1, 1);
	V ia= (V) a;

	@Before
	public void setUp()
	{
		graf=new Graph(100);
		InfoVer b= new InfoVer(2,2,2,2);
		V ib = (V)b;
		InfoVer c= new InfoVer (3,3,3,3);
		V ic = (V)c;
		InfoVer d= new InfoVer(4,4,4,4);
		V id = (V)d;

		graf.addVertex(1, ia);		
		graf.addVertex(2, ib);
		graf.addVertex(3, ic);
		graf.addVertex(4, id);
		graf.addEdge(1, 2, 100, 23);
		graf.addEdge(1, 4, 200, 24);
		graf.addEdge(2, 3, 700, 34);
	}
	@Before
	public void setUp1()
	{
		graf=new Graph(100);
	}
	
	@Test
	public void testVacio()
	{
		setUp1();
		assertEquals(false, graf.tieneV(1));
		assertEquals(0, graf.darV());
		assertEquals(0, graf.darE());
		assertNull(graf.getInfoVertex(1));
	}
	
	@Test
	public void graap()
	{
		setUp();
		assertEquals(true, graf.tieneV(1));
		assertEquals(3, graf.darE());
		assertEquals(4, graf.darV());
		assertEquals(ia, graf.getInfoVertex(1));
		Vertice v=  (Vertice) graf.darHash().get(1);
		assertEquals(2,v.numVEC() );
	}
	

	
	@Test
	public void dfs()
	{
		setUp();
		graf.dfs(1);
		Vertice v=  (Vertice) graf.darHash().get(1);
		Vertice v2=  (Vertice) graf.darHash().get(2);
		Vertice v3=  (Vertice) graf.darHash().get(3);
		Vertice v4=  (Vertice) graf.darHash().get(4);
		assertEquals(true, v.esVisitado());
		assertEquals(true, v2.esVisitado());
		assertEquals(true, v3.esVisitado());
		assertEquals(true, v4.esVisitado());

	}
	
	@Test 
	public void cc()
	{
		setUp();
		int cu=graf.cc();
		assertEquals(1, cu);
	}
	
}
