package test.logic;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.HashLinearProbing;
import model.logic.TravelTime;

public class testHashLinearProbing {

	private HashLinearProbing<String, TravelTime> hash;
	String llave;
	TravelTime uno;
	String llave3;
	
	@Before
	public void setUp()
	{
		hash= new HashLinearProbing<String, TravelTime>(3);
		uno= new TravelTime(1, 1, 2, 2, 3.5, 4.5, 3.2, 1.1 );
		llave= uno.darTrim()+"-"+uno.darSourceid()+"-"+ uno.darDtid();
		TravelTime dos= new TravelTime(2, 28, 2, 2, 1, 4.5, 3.2, 1.1 );
		String llave2= dos.darTrim()+"-"+dos.darSourceid()+"-"+ dos.darDtid();
		//EL VIAJE TRES TIENE LA MAYOR HORA
		TravelTime tres= new TravelTime(1, 36, 2, 5, 8, 4.5, 3.2, 1.1 );
		llave3= tres.darTrim()+"-"+tres.darSourceid()+"-"+ tres.darDtid();
		TravelTime cuatro= new TravelTime(2, 101, 2, 2, 20, 4.5, 3.2, 1.1 );
		String llave4= cuatro.darTrim()+"-"+cuatro.darSourceid()+"-"+ cuatro.darDtid();
		TravelTime cinco= new TravelTime(1, 11, 2, 1, 18, 4.5, 3.2, 1.1 );
		String llave5= cinco.darTrim()+"-"+cinco.darSourceid()+"-"+ cinco.darDtid();
		TravelTime seis= new TravelTime(2, 4, 2, 2, 28, 4.5, 3.2, 1.1 );
		String llave6= seis.darTrim()+"-"+seis.darSourceid()+"-"+ seis.darDtid();
		hash.putInSet(llave, uno);
		hash.putInSet(llave2, dos);
		hash.putInSet(llave3, tres);
		hash.putInSet(llave4, cuatro);
		hash.putInSet(llave5, cinco);
		hash.putInSet(llave6, seis);

	}
	@Before
	public void setUp2()
	{
		hash= new HashLinearProbing<String, TravelTime>();
	}
	
	@Test
	//CREA UNA TABLA Y LUEGO ELIMINA UN DATO
	public void agregarYEliminar()
	{
		setUp();
		//total viajes
		assertEquals(6, hash.darDatos());
		assertEquals(2, hash.darRehashes());
		//Saca un elemento
		hash.deleteSet(llave);
		assertEquals(5, hash.darDatos());
	}
	
	@Test
	//BUSCA UN SET
	public void getSet()
	{
		setUp();
		assertEquals(uno, hash.getSet(llave).next());
	}
	
	@Test
	//TABLA VAC�A
	public void vacio()
	{
		setUp2();
		assertEquals(0, hash.darDatos());
		assertEquals(true, hash.esVacio());
	}
	
	@Test
	//ENCUENTRA LA PRIMERA LLAVE DE LA COLA DE LLAVES QUE DEVUELVE EL M�TODO (ORDENADAS POR HORA)
	public void llaves()
	{
		setUp();
		assertEquals(llave3, hash.keys().next());

	}
	
}